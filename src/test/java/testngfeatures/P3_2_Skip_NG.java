package testngfeatures;

import org.testng.SkipException;
import org.testng.annotations.Test;

public class P3_2_Skip_NG {

	@Test(enabled = true)
	public void first(){
		System.out.println("Inside first()  ");

	}

	@Test (enabled = false)
	public void second(){
		System.out.println("Inside second()  ");

	}
	@Test(enabled = true) 
	public void third(){
		System.out.println("Inside third()  ");
		

	}

	@Test
	public void fourth(){
		System.out.println("Inside fourth()  ");
		throw new SkipException("Decided for Skipping - we are not ready for execution of this test.");

	}

	@Test(enabled = false) 
	public void fifth(){
		System.out.println("Inside fifth()  ");

	}


}
