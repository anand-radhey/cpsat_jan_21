package testngfeatures;

import org.testng.SkipException;
import org.testng.annotations.Test;

public class P4_Skip_testng_NG2 {

	@Test
	public void first(){
		System.out.println("Inside first()  ");

	}

	@Test 
	public void second(){
		System.out.println("Inside second()  ");

	}
	@Test
	public void third(){
		System.out.println("Inside third()  ");
		

	}

	@Test
	public void fourth(){
		System.out.println("Inside fourth()  ");
		throw new SkipException("Decided for Skipping - we are not ready for execution of this test.");

	}

	@Test 
	public void fifth(){
		System.out.println("Inside fifth()  ");

	}


}
