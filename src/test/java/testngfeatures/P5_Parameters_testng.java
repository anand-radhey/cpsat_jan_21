package testngfeatures;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class P5_Parameters_testng {

	@Test
	@Parameters({"param1", "param2" })
	public void first(@Optional("USA") String name1, @Optional("India") String name2){
		System.out.println("Inside first()  ");
		System.out.println("Parameters passed from testng.xml are : \n"+name1 + "\t" + name2);
		

	}

	@Test 
	public void second(){
		System.out.println("Inside second()  ");

	}
	@Test 
	@Parameters({"browser"})
	public void third(@Optional("Safari") String myBrowser){
		System.out.println("Inside third()  ");
		System.out.println("Parameters passed from testng.xml are : \n"+myBrowser);
		//driver = utils.HelperFunctions.createAppropriateDriver(myBrowser);
		

	}

	@Test 
	public void fourth(){
		System.out.println("Inside fourth()  ");

	}

	@Test 
	@Parameters({"hostURL", "param2"})
	public void fifth(@Optional("wikipedia")String url, @Optional("USA") String country){
		System.out.println("Inside fifth()  ");
		System.out.println("Parameters passed from testng.xml are : \n"+url +"\t" + country);

	}


}
