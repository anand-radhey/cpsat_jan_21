package testngfeatures;

import org.testng.annotations.Test;

public class P2_Priority_NG {

	@Test(priority= 0)
	public void first(){
		System.out.println("Inside first()  ");

	}

	@Test (priority= 1)
	public void second(){
		System.out.println("Inside second()  ");

	}
	@Test(priority= -5) 
	public void third(){
		System.out.println("Inside third()  ");
		

	}

	@Test(priority= 3) 
	public void fourth(){
		System.out.println("Inside fourth()  ");

	}

	@Test(priority= 3)
	public void fifth(){
		System.out.println("Inside fifth()  ");

	}


}
