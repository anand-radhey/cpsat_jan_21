package day8;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class Ex11_Jquery {
	WebDriver driver;
	
  @Test
  public void testMethod() {
	  
	  driver.get("https://jqueryui.com/autocomplete/");
	  
	//*[@id="tags"]
	  
	  driver.switchTo().frame(0);
	  
	  
	  WebElement weinput = driver.findElement(By.xpath("//*[@id=\"tags\"]"));
	  weinput.sendKeys("J");
	  
	  
	//*[@class='ui-menu-item']
	  
	  By byvar = By.xpath("//*[@class='ui-menu-item']");
	  List<WebElement> lst = driver.findElements(byvar);
	  WebElement myele = null;
	  for (WebElement  ele      : lst) {
		  
		  System.out.println( ele.getText());
		  if(ele.getText().equalsIgnoreCase("Java")) {
			 myele = ele; 
			//ele.click();
		  }
		  
	  }
	  myele.click();
	  
	  
	  
	  
	  
	  
  }
  @BeforeTest
  public void beforeTest() {
	  driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
	  
  }

  @AfterTest
  public void afterTest() {
	  try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  driver.quit();
  }

}
