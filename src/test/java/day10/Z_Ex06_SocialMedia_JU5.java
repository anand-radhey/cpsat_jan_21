package day10;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

//import utils.HelperFunctions;

public class Z_Ex06_SocialMedia_JU5 {

	WebDriver driver ;
	
	//@BeforeTest
	@BeforeEach
	public void setUp() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		
		driver = new ChromeDriver();
		
		//driver = HelperFunctions.createAppropriateDriver("chrome",true);
		//driver = HelperFunctions.createAppropriateDriver("fireFox",false);
		
		
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@AfterEach
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() {
		//fail("Not yet implemented");
		driver.get("http://agiletestingalliance.org/");
		//*[@id="custom_html-10"]/div/ul/li[*]/a
		
		By bySocialMedia = By.xpath("//*[@id=\"custom_html-10\"]/div/ul/li[*]/a");
		
		List<WebElement> list = driver.findElements(bySocialMedia);
		
		int size = list.size();
		
		for (int i=0;i<size;i++) {
			
			WebElement element = list.get(i);
			String href = element.getAttribute("href");
			System.out.println(href);
		}
		
		//HelperFunctions.captureScreenShot(driver, "src//test//resources//screenshots//filename.jpg");
	}

}