package day10;

//import org.testng.annotations.Test;

import utils.HelperFunctions;

//import org.testng.annotations.DataProvider;
//import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.testng.Assert;
//import org.testng.annotations.AfterTest;

public class Ex07_IMDB_DP_JU5 {
	WebDriver driver=null;

	//@BeforeTest
	@BeforeEach
	public void setup() {
		System.out.println("Entering setup() ");
		driver = HelperFunctions.createAppropriateDriver("Chrome");

		System.out.println("Exit setup() ");
	}
	
	  //@DataProvider
	// method source should be static
	  public static Object[][] mydp() throws EncryptedDocumentException, InvalidFormatException, IOException {
		  String[][] mdata = utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\imdbdata.xlsx", "data");
		  	  /*{
			  				{ "Baazigar", "Mastan", "Shah Rukh Khan"},
				  			{"Raazi", "Meghna Gulzar", "Vicky"}
		  					};*/
		  return mdata;
	  }
	
  //@Test(dataProvider = "mydp")
	  @ParameterizedTest
	  @MethodSource("mydp")
	public void movieTest(String v1, String v2, String v3) {
		System.out.println("Entering movieTest() ");
		System.out.println(" v1 "+ v1 + "\n v2 "+ v2+ "\n v3 "+ v3);


		String movie = v1; // "Baazigar";
		String expDir = v2; //"AMastan";
		String expStar = v3; // "Shah Rukh Khan";
		

		
		driver.get("https://www.imdb.com/");
	
		By byinput = By.id("suggestion-search");

		WebElement weinput = driver.findElement(byinput);

		weinput.sendKeys(movie);
		weinput.sendKeys(Keys.ENTER);

		System.out.println("Page 2 Title : "+ driver.getTitle());


		driver.findElement(By.partialLinkText(movie)).click();

		System.out.println("Page 3 Title : "+ driver.getTitle());


		List<WebElement>  dirList = driver.findElements(By.xpath("//*[contains(text(), 'Director')]/following-sibling::*"));

		boolean dirflag = false;

		String msg = "";

		for(WebElement   dir   : dirList) {
			if (dir.getText().contains(expDir)) {
				System.out.println(" Found Expected Dir " + expDir);
				dirflag = true;
				break;
			}
		}

		if(dirflag) {
			System.out.println(" Found Expected Dir " + expDir);
			msg = msg + " Found Expected Dir " + expDir + ".\n ";
		} else {
			System.out.println(" NOT Found Expected Dir " + expDir);
			msg = msg + " NOT Found Expected Dir " + expDir + ".\n ";
		}

		// To process for Star
		List<WebElement>  starList = driver.findElements(By.xpath("//*[contains(text(), 'Stars')]/following-sibling::*"));

		boolean starflag = false;

		//String msg = "";

		for(WebElement   star   : starList) {
			if (star.getText().contains(expStar)) {
				System.out.println(" Found Expected star " + expStar);
				starflag = true;
				break;
			}
		}

		if(starflag) {
			System.out.println(" Found Expected star " + expStar);
			msg = msg + " Found Expected Star " + expStar + ".\n ";
		} else {
			System.out.println(" NOT Found Expected Star " + expStar);
			msg = msg + " NOT Found Expected Star " + expStar + ".\n ";
		}


		//Assert.assertEquals(dirflag && starflag, true, msg);
		Assertions.assertEquals(true, dirflag && starflag, msg);

		System.out.println("Exit movieTest() ");

	}


 
  //@AfterTest
  @AfterEach
	public void teardown() {
		System.out.println("Entering teardown() ");

		driver.quit();
		System.out.println("Exit teardown() ");

	}

}
