package agriddemotests;

import org.testng.annotations.Test;

import utils.HelperFunctions;
import utils.RemoteBrowserCreation;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class RemoteATAExample {

	WebDriver driver;

	@Parameters({"browser","host","port"})
	@BeforeTest
	public void beforeTest(@Optional("chrome") String browser,
			@Optional("http://192.168.29.143") String host, 
			@Optional("4444")String port) throws MalformedURLException, InterruptedException {
		//driver = BrowserCreation.createRemoteDriver("chrome", "http://164.52.193.178", "4444");
		
		System.out.println("browser =" + browser);
		System.out.println("host =" + host);
		System.out.println("port =" + port);

		/*
			some times testng optional parameters - gives extra "quotes"
			// we can try removing extra quotes
		 */
		
		
		browser = browser.replace("\"", "");
		host = host.replace("\"", "");
		port = port.replace("\"", "");

		System.out.println("browser =" + browser);
		System.out.println("host =" + host);
		System.out.println("port =" + port);

		System.out.println("creating remote webdriver");
		
		driver = RemoteBrowserCreation.createRemoteDriver(browser, host , port);
		
		//driver = new ChromeDriver();
		
		// RemoteWebDriver
		// RemoteWebDriver (URL of the hub or the server where the hub is there)
		
		//driver = new ChromeDriver();

	}


	@Test
	public void f() {

		driver.get("file:///C:/ZVA/CPSAT/mavenCPSATjan2021/src/test/resources/data/dropdown.html");
		
		WebElement weselect = driver.findElement(By.xpath("/html/body/form/select"));
		
		Select selobj = new Select(weselect);
		
		List<WebElement>   lstoptions = selobj.getOptions();
		
		for(WebElement   ele : lstoptions) {
			System.out.println(ele.getText());
		}
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		selobj.selectByValue("41");
		
		
		
			
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		selobj.selectByVisibleText("CP-MAT");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}



	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}
