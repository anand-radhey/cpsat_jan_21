package day7;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex07_IMDB_NG {
	WebDriver driver=null;

	@BeforeTest
	public void setup() {
		System.out.println("Entering setup() ");
		driver = HelperFunctions.createAppropriateDriver("Chrome");

		System.out.println("Exit setup() ");
	}


	@Test
	public void movieTest() {
		System.out.println("Entering movieTest() ");


		String movie = "Baazigar";
		String expDir = "AMastan";
		String expStar = "Shah Rukh Khan";

		driver.get("https://www.imdb.com/");
		//*[@id="suggestion-search"]
		//By byinput = By.xpath("//*[@id=\"suggestion-search\"]");
		//By byinput = By.xpath("//*[@name='q']");
		//By byinput = By.name("q");
		By byinput = By.id("suggestion-search");

		WebElement weinput = driver.findElement(byinput);

		weinput.sendKeys(movie);
		weinput.sendKeys(Keys.ENTER);

		System.out.println("Page 2 Title : "+ driver.getTitle());

		//*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a

		//By bylink = By.partialLinkText(movie);
		driver.findElement(By.partialLinkText(movie)).click();

		System.out.println("Page 3 Title : "+ driver.getTitle());


		//*[@id="title-overview-widget"]/div[2]/div[1]/div[2]/h4       Directors

		//*[contains(text(), 'Directors'     )]/following-sibling::*

		List<WebElement>  dirList = driver.findElements(By.xpath("//*[contains(text(), 'Directors')]/following-sibling::*"));

		boolean dirflag = false;

		String msg = "";

		for(WebElement   dir   : dirList) {
			if (dir.getText().contains(expDir)) {
				System.out.println(" Found Expected Dir " + expDir);
				dirflag = true;
				break;
			}
		}

		if(dirflag) {
			System.out.println(" Found Expected Dir " + expDir);
			msg = msg + " Found Expected Dir " + expDir + ".\n ";
		} else {
			System.out.println(" NOT Found Expected Dir " + expDir);
			msg = msg + " NOT Found Expected Dir " + expDir + ".\n ";
		}

		// To process for Star
		List<WebElement>  starList = driver.findElements(By.xpath("//*[contains(text(), 'Stars')]/following-sibling::*"));

		boolean starflag = false;

		//String msg = "";

		for(WebElement   star   : starList) {
			if (star.getText().contains(expStar)) {
				System.out.println(" Found Expected star " + expStar);
				starflag = true;
				break;
			}
		}

		if(starflag) {
			System.out.println(" Found Expected star " + expStar);
			msg = msg + " Found Expected Star " + expStar + ".\n ";
		} else {
			System.out.println(" NOT Found Expected Star " + expStar);
			msg = msg + " NOT Found Expected Star " + expStar + ".\n ";
		}


		Assert.assertEquals(dirflag && starflag, true, msg);

		System.out.println("Exit movieTest() ");

	}

	@AfterTest
	public void teardown() {
		System.out.println("Entering teardown() ");

		driver.quit();
		System.out.println("Exit teardown() ");

	}

}
