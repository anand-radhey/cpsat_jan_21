package day6;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class Ex03_AnnaUniv {

	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("Chrome", true);
		
		/*WebDriver driver;
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");

		ChromeOptions chromeOptions = new ChromeOptions();
		
		
		chromeOptions.setHeadless(true);

		driver = new ChromeDriver(chromeOptions);	
		*/
		
		
		driver.get("http://www.annauniv.edu/department/index.php");
		
		//*[@id="link3"]/strong
		By byvar1 = By.xpath("//*[@id='link3']/strong");
		WebElement wecivil = driver.findElement(byvar1);
		//wecivil.click();
		
		//*[@id="menuItemHilite33"]
		
		
		By byvar2 = By.xpath("//*[@id=\"menuItemHilite33\"]");
		WebElement weiom = driver.findElement(byvar2);
		
		Actions act = new Actions(driver);
		
		//act.moveToElement(wecivil).moveToElement(weiom).click().build().perform();
		act.moveToElement(wecivil).moveToElement(weiom).click().perform();;
		
		System.out.println(driver.getTitle());
		
		//File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		String filename = "src\\test\\resources\\screenshots\\Anna_HL002.png";
		
		utils.HelperFunctions.captureScreenShot(driver, filename);
		
		
		
		//driver.quit();
		
		
		
		
		
		
		
		
		
		

	}

}
