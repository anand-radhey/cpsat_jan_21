package day6;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ex04_AlertsHandling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrOME");
		WebDriverWait wt = new WebDriverWait(driver, 15);
		
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");
		
		//*[@id="content"]/div/ul/li[1]/button
		WebElement webutton = driver.findElement(By.xpath("//*[@id=\"content\"]/div/ul/li[1]/button"));
		
		//wt.until(ExpectedConditions.alertIsPresent());
		
		webutton.click();
	
		wt.until(ExpectedConditions.alertIsPresent());
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
		//*[@id="result"]
	String expRes = "You successfuly clicked an alert";
		
		String actRes = driver.findElement(By.xpath("//*[@id=\"result\"]")).getText();
		System.out.println();
		if (actRes.equals(expRes)) {
			System.out.println("The " +expRes + " Expected correct message");
		}
		
		
		driver.quit();

	}

}
