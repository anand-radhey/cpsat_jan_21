package day4;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Google {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		//                 "webdriver.gecko.driver"
		
		WebDriver driver = new ChromeDriver();  
		// Inheritance
		// Interface     Class   - How these are related? evidence
		//driver.get("www.google.com");
		driver.get("https://www.google.com");
		String pageTitle = driver.getTitle();
		
		System.out.println(pageTitle);
		By byvar = By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[1]/div/div[2]/input");
		//      /html/body/div[1]/div[3]/form/div[2]/div[1]/div[1]/div/div[2]/input
		//By byvar = By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input");
		
		WebElement inputbox = driver.findElement(byvar);
		
		inputbox.sendKeys("Selenium");
		inputbox.sendKeys(Keys.ENTER);
		
		
		
		//driver.close();
		driver.quit();
		
		
		
		

	}

}
