package day4;

public class MultiDimArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*
	      Q1	Q2	    Q3	     Q4
2013	1000	5000	6000	3000
2014	2000	6000	3000	7500
2015	2100	2700	5000	1870
*/
		
	int[]	 yr1 = { 1000,	5000,	6000,	3000 };
	int[]    yr2 = {2000,	6000,	3000,	7500 };
	int[]    yr3 = {2100,	2700,	5000,	1870};
	
	int[][]  salesdata =   {yr1, 
							yr2, 
							yr3};
	int nrows = salesdata.length;  // 3   4  12
	int ncols = salesdata[0].length  ;      //  yr1.length;  4
	
	//salesdata[0][0]
	System.out.println("No. of Rows =" + nrows);
	System.out.println("No. of Cols =" + ncols);
	
	System.out.println("  Q1\t\t" +"Q2  \t\t"+ "Q3 \t\t"+ "Q4   "  );
	
	for ( int i = 0; i < nrows ; i++) {
		for ( int j = 0; j < ncols; j++) {
				System.out.print(salesdata[i][j] + "   |   ");
			
		}
		System.out.println();
		
		
	}
	
	
	
	
		
		
	}

}
