package day5;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GoogleNavigation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrOME");
		//System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
		
		//WebDriver driver = new FirefoxDriver();
		
		driver.get("https://www.google.com");
		driver.manage().window().maximize();
		//     /html/body/div[1]/div[3]/form/div[2]/div[1]/div[1]/div/div[2]/input
		//*[@name="q"]
		
		//By byvar = By.xpath("/html/body/div[1]/div[3]/form/div[2]/div[1]/div[1]/div/div[2]/input");
		//By byvar = By.xpath("//*[@name=\"q\"]");
		
		By byvar = By.name("q");
		
		
		WebElement weinput = driver.findElement(byvar);
		
		weinput.sendKeys("CPSAT");
		weinput.sendKeys(Keys.ENTER);
		
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Page 2 Title " + driver.getTitle());
		
		//*[@id="rso"]/div[1]/div/div[2]/div[1]/a/h3/span
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.navigate().back();
		System.out.println("Page 2 - 1 backward Title " + driver.getTitle());
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.navigate().forward();
		System.out.println("Page 1 - 2 forward Title " + driver.getTitle());
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		By byvar2 = By.xpath("/html/body/div[7]/div[2]/div[9]/div[2]/div/div[2]/div[2]/div/div/div[1]/div/div[2]/div[1]/a/h3/span");
		                     // /html/body/div[7]/div[2]/div[9]/div[2]/div/div[2]/div[2]/div/div/div[1]/div/div[2]/div[1]/a/h3/span
		driver.findElement(byvar2).click();
		
		System.out.println("Page 3 Title " + driver.getTitle());
		
		*/
		
		
		driver.quit();
		
		
		
		
		
		
		

	}

}
