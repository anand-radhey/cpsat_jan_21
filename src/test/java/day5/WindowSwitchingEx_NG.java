package day5;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;

public class WindowSwitchingEx_NG {
	
	WebDriver driver = null;

	@BeforeTest
	public void setup() {
		System.out.println("Entering setup() ");
		System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");

		driver = new FirefoxDriver();  
		driver.manage().window().maximize();
		System.out.println("Exiting setup() ");
	}


	@Test
	public void testFunction() {
		System.out.println("Entering testFunction() ");
		driver.get("https://www.ataevents.org/");
		By byvar = By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[*]/div/div/div/div/div/figure/a");
		List<WebElement>  welist =   driver.findElements(byvar);
		for( WebElement   ele  : welist) {
			ele.click();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		Set<String> setwinhandles = driver.getWindowHandles();
		for( String  handle       : setwinhandles  ) {
			System.out.println(handle);
			driver.switchTo().window(handle);
			System.out.println(driver.getTitle());
			if( driver.getTitle().contains("CP-WST")){
				System.out.println("CP-WST window");
				break;
			}

		}



		System.out.println("Exiting testFunction() ");

	}



	@AfterTest
	public void cleanup() {
		System.out.println("Entering cleanup() ");
		driver.quit();
		System.out.println("Exiting cleanup() ");
	}

}
