package day5;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WindowSwitchingEx {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
			
		WebDriver driver = new FirefoxDriver();  
		driver.manage().window().maximize();
		
		driver.get("https://www.ataevents.org/");
		
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/figure/a
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/div/h3/a
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[3]/div/div/div/div/div/figure/a
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[4]/div/div/div/div/div/figure/a
		
		By byvar = By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[*]/div/div/div/div/div/figure/a");
		List<WebElement>  welist =   driver.findElements(byvar);
		//List<String>
		
		for( WebElement   ele  : welist) {
			ele.click();
			Thread.sleep(3000);
			
		}
		
		Set<String> setwinhandles = driver.getWindowHandles();
		
		for( String  handle       : setwinhandles  ) {
			System.out.println(handle);
			driver.switchTo().window(handle);
			System.out.println(driver.getTitle());
			if( driver.getTitle().contains("CP-WST")){
				System.out.println("CP-WST window");
				break;
				
			}
			
		}
		
		
		

	}

}
