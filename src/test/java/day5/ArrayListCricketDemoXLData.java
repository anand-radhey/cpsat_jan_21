package day5;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class ArrayListCricketDemoXLData {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		Player searchPlayer = new Player("R Jadeja",	"IN14", 127);

		ArrayList<Player> arrlist = new ArrayList<Player>();
		String fileName = "src\\test\\resources\\data\\Cricket_Score_India_Eng_SA.xlsx";
		String[] sheets = {"SA","India", "England"};
		boolean match = false;
		for( String  sh     : sheets) {
			String[][] datafromxl = utils.XLDataReaders.getExcelData(fileName, sh);

			for(int i =0; i < datafromxl.length ; i++) {
				String name = datafromxl[i][0];
				String id = datafromxl[i][1];
				int score = Integer.parseInt(datafromxl[i][2]);

				Player player = new Player(name, id, score);
				//System.out.println(player);
				arrlist.add(player);
			}

		}

		for( Player   p     : arrlist) {
			String plName = p.getName();
			String plId = p.getId();
			int plScore = p.getScore();

			String splName = searchPlayer.getName();
			String splId = searchPlayer.getId();
			int splScore = searchPlayer.getScore();

			if( plName.equals(splName)    &&   plId.equals(splId)   &&   plScore == splScore     ) {
				match = true;
				break;
			}
			//System.out.println(p);
		}

		if(match) {
			System.out.println(searchPlayer  + " Found in the XL File");
		} else {
			System.out.println(searchPlayer  + " NOT Found in the XL File");
		}


	}

}
